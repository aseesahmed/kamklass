@extends('admin/layout')
@section('page_title', 'Coupon')
@section('coupon_select', 'active')
@section('container')

<div class="col-lg-12">
    @if(session()->has('message'))
        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
            <span class="badge badge-pill badge-success">Success</span>
            {{session('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif
    <h1>Coupon</h1>
    <br/>
    <a href="{{url('admin/coupon/manage_coupon')}}">
        <button type="button" class="btn btn-success">Add Coupon</button>
    </a>
    <br/>
    <br/>
    
    <div class="table-responsive table--no-card m-b-30">
        <table class="table table-borderless table-striped table-earning">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Coupon Title</th>
                    <th>Coupon Code</th>
                    <th>Coupon Value</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $list)
                <tr>
                    <td>{{$list->id}}</td>
                    <td>{{$list->title}}</td>
                    <td>{{$list->code}}</td>
                    <td>{{$list->value}}</td>
                    <td>
                        <a href="{{url('admin/coupon/manage_coupon/')}}/{{$list->id}}"><button type="button" class="btn btn-warning">Edit</button></a>
                        @if($list->status==1)
                            <a href="{{url('admin/coupon/status/0')}}/{{$list->id}}"><button type="button" class="btn btn-info">Active</button></a>
                            @elseif($list->status==0)
                                <a href="{{url('admin/coupon/status/1')}}/{{$list->id}}"><button type="button" class="btn btn-info">Deactive</button></a>
                        @endif
                        <a href="{{url('admin/coupon/delete/')}}/{{$list->id}}"><button type="button" class="btn btn-danger">Delete</button></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection