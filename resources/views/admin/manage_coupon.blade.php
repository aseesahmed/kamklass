@extends('admin/layout')
@section('page_title', 'Manage Coupon')
@section('coupon_select', 'active')
@section('container')

<div class="col-lg-12">
    <h1>Manage Coupon</h1>
    <br/>
    <a href="{{url('admin/coupon')}}">
        <button type="button" class="btn btn-success">Back</button>
    </a>
    <br/>
    <br/>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <form action="{{route('category.manage_coupon_process')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="title" class="control-label mb-1">Coupon Title</label>
                        <input id="title" value="{{$title}}" name="title" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                        @error('title')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="code" class="control-label mb-1">Coupon Code</label>
                        <input id="code" value="{{$code}}" name="code" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                        @error('code')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="value" class="control-label mb-1">Coupon Value</label>
                        <input id="value" value="{{$value}}" name="value" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                        @error('value')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="type" class="control-label mb-1">Coupon Type</label>
                        <select name="type" id="type" class="form-control" required>
                            @if($type=='Value')
                            <option value="Value" selected>Value</option>
                            <option value="Per">Percentage</option>
                            @elseif($type=='Per')
                            <option value="Value">Value</option>
                            <option value="Per" selected>Percentage</option>
                            @else
                            <option value="Value">Value</option>
                            <option value="Per">Percentage</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="min_order_amt" class="control-label mb-1">Coupon Minimum Order Amount</label>
                        <input id="min_order_amt" value="{{$min_order_amt}}" name="min_order_amt" type="number" class="form-control" aria-required="true" aria-invalid="false">
                    </div>
                    <div class="form-group">
                        <label for="is_one_time" class="control-label mb-1">Coupon is One Time</label>
                        <select name="is_one_time" id="is_one_time" class="form-control" required>
                            @if($is_one_time=='0')
                            <option value="0" selected>No</option>
                            <option value="1">Yes</option>
                            @elseif($is_one_time=='1')
                            <option value="0">No</option>
                            <option value="1" selected>Yes</option>
                            @else
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                            @endif
                        </select>
                    </div>

                    <div>
                        <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                            <i class="fa fa-list-alt"></i>&nbsp;
                            <span id="payment-button-amount">Add Coupon</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                    </div>
                    <input type="hidden" name="id" value={{$id}} />
                </form>
            </div>
        </div>
    </div>
</div>
@endsection