@extends('admin/layout')
@section('page_title', 'Manage Size')
@section('size_select', 'active')
@section('container')

<div class="col-lg-12">
    <h1>Manage Size</h1>
    <br/>
    <a href="{{url('admin/size')}}">
        <button type="button" class="btn btn-success">Back</button>
    </a>
    <br/>
    <br/>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <form action="{{route('size.manage_size_process')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="size" class="control-label mb-1">Category</label>
                        <input id="size" value="{{$size}}" name="size" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                        @error('size')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                    </div>

                    <div>
                        <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                            <i class="fa fa-list-alt"></i>&nbsp;
                            <span id="payment-button-amount">Add Size</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                    </div>
                    <input type="hidden" name="id" value={{$id}} />
                </form>
            </div>
        </div>
    </div>
</div>
@endsection