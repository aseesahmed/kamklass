@extends('admin/layout')
@section('page_title', 'Manage Category')
@section('category_select', 'active')
@section('container')

<div class="col-lg-12">
    <h1>Manage Category</h1>
    <br/>
    <a href="{{url('admin/category')}}">
        <button type="button" class="btn btn-success">Back</button>
    </a>
    <br/>
    <br/>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <form action="{{route('category.manage_category_process')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="category_name" class="control-label mb-1">Category</label>
                        <input id="category_name" value="{{$category_name}}" name="category_name" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                        @error('category_name')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="category_slug" class="control-label mb-1">Category Slug</label>
                        <input id="category_slug" value="{{$category_slug}}" name="category_slug" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                        @error('category_slug')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="parent_category_id" class="control-label mb-1">Parent Category</label>
                        <select name="parent_category_id" id="parent_category_id" class="form-control">
                            <option value="">Select Category</option>
                            @foreach($category as $list)
                                @if($parent_category_id==$list->id)
                                <option selected value="{{$list->id}}">{{$list->category_name}}</option>
                                @else
                                <option value="{{$list->id}}">{{$list->category_name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="category_image" class="control-label mb-1">Category Image</label>
                        <input id="category_image" name="category_image" type="file" class="form-control" aria-required="true" aria-invalid="false">
                        @error('category_image')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                        @if($category_image!='')
                            <a href="{{asset('storage/media/category/'.$category_image)}}" target=_blank>
                                <img width="100px" src="{{asset('storage/media/category/'.$category_image)}}" alt="">
                            </a>
                        @endif
                    </div>
                    <div>
                        <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                            <i class="fa fa-list-alt"></i>&nbsp;
                            <span id="payment-button-amount">Add Category</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                    </div>
                    <input type="hidden" name="id" value={{$id}} />
                </form>
            </div>
        </div>
    </div>
</div>
@endsection