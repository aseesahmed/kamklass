@extends('admin/layout')
@section('page_title', 'Manage Product')
@section('product_select', 'active')
@section('container')


@if($id>0)
    {{$image_required=""}}
    @else
    {{$image_required="required"}}
@endif
<div class="col-lg-12">
    <h1>Manage Product</h1>
    @if(session()->has('sku_error'))
    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
        {{session('sku_error')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    @endif

    @error('attr_image.*')
    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
        {{$message}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    @enderror

    @error('images.*')
    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
        {{$message}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    @enderror

    <br/>
    <a href="{{url('admin/product')}}">
        <button type="button" class="btn btn-success">Back</button>
    </a>
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <br/>
    <br/>
    <div class="col-lg-10">
        <form action="{{route('product.manage_product_process')}}" method="post" enctype="multipart/form-data">
            <div class="card">
                <div class="card-body">
                    
                        @csrf
                        <div class="form-group">
                            <label for="name" class="control-label mb-1">Product Name</label>
                            <input id="name" value="{{$name}}" name="name" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                            @error('name')
                            <div class="alert alert-danger" role="alert">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="slug" class="control-label mb-1">Product Slug</label>
                            <input id="slug" value="{{$slug}}" name="slug" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                            @error('slug')
                            <div class="alert alert-danger" role="alert">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="image" class="control-label mb-1">Product Image</label>
                            <input id="image" name="image" type="file" class="form-control" aria-required="true" aria-invalid="false" {{$image_required}}>
                            @error('image')
                            <div class="alert alert-danger" role="alert">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="category_id" class="control-label mb-1">Category ID</label>
                                    <select name="category_id" id="category_id" class="form-control" required>
                                        <option value="">Select Category</option>
                                        @foreach($category as $list)
                                            @if($category_id==$list->id)
                                            <option selected value="{{$list->id}}">{{$list->category_name}}</option>
                                            @else
                                            <option value="{{$list->id}}">{{$list->category_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="brand" class="control-label mb-1">Brand</label>
                                    <select name="brand" id="brand" class="form-control" required>
                                        <option value="">Select Brand</option>
                                        @foreach($brands as $list)
                                            @if($brand==$list->id)
                                            <option selected value="{{$list->id}}">{{$list->name}}</option>
                                            @else
                                            <option value="{{$list->id}}">{{$list->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <!-- <div class="col-md-4">
                                    <label for="brand" class="control-label mb-1">Product Brand</label>
                                    <input id="brand" value="{{$brand}}" name="brand" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                </div> -->
                                <div class="col-md-4">
                                    <label for="model" class="control-label mb-1">Product Model</label>
                                    <input id="model" value="{{$model}}" name="model" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                </div>
                            </div>
                            
                        </div>
    
                        <div class="form-group">
                            <label for="short_desc" class="control-label mb-1">Short Description</label>
                            <textarea name="short_desc" id="short_desc" class="form-control" cols="30" rows="2">{{$short_desc}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="desc" class="control-label mb-1">Description</label>
                            <textarea name="desc" id="desc" class="form-control" cols="30" rows="7">{{$desc}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="keywords" class="control-label mb-1">Keywords</label>
                            <textarea name="keywords" id="keywords" class="form-control" cols="30" rows="2">{{$keywords}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="technical_specification" class="control-label mb-1">Technical Specification</label>
                            <textarea name="technical_specification" id="technical_specification" class="form-control" cols="30" rows="2">{{$technical_specification}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="uses" class="control-label mb-1">Uses</label>
                            <textarea name="uses" id="uses" class="form-control" cols="30" rows="2">{{$uses}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="warranty" class="control-label mb-1">Warranty</label>
                            <textarea name="warranty" id="warranty" class="form-control" cols="30" rows="2">{{$warranty}}</textarea>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="lead_time" class="control-label mb-1">Lead Time</label>
                                    <input id="lead_time" value="{{$lead_time}}" name="lead_time" type="text" class="form-control" aria-required="true" aria-invalid="false">
                                </div>
                                <div class="col-md-4">
                                    <label for="tax_id" class="control-label mb-1">Tax</label>
                                    <select name="tax_id" id="tax_id" class="form-control" required>
                                        <option value="">Select Tax</option>
                                        @foreach($taxes as $list)
                                            @if($tax_id==$list->id)
                                            <option selected value="{{$list->id}}">{{$list->tax_desc}}</option>
                                            @else
                                            <option value="{{$list->id}}">{{$list->tax_desc}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="is_promo" class="control-label mb-1">Is Promo</label>
                                    <select name="is_promo" id="is_promo" class="form-control" required>
                                        @if($is_promo=='0')
                                        <option value="0" selected>No</option>
                                        <option value="1">Yes</option>
                                        @elseif($is_promo=='1')
                                        <option value="0">No</option>
                                        <option value="1" selected>Yes</option>
                                        @else
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="is_featured" class="control-label mb-1">Is Featured</label>
                                    <select name="is_featured" id="is_featured" class="form-control" required>
                                        @if($is_featured=='0')
                                        <option value="0" selected>No</option>
                                        <option value="1">Yes</option>
                                        @elseif($is_featured=='1')
                                        <option value="0">No</option>
                                        <option value="1" selected>Yes</option>
                                        @else
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="is_discounted" class="control-label mb-1">Is Discounted</label>
                                    <select name="is_discounted" id="is_discounted" class="form-control" required>
                                        @if($is_discounted=='0')
                                        <option value="0" selected>No</option>
                                        <option value="1">Yes</option>
                                        @elseif($is_discounted=='1')
                                        <option value="0">No</option>
                                        <option value="1" selected>Yes</option>
                                        @else
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="is_trending" class="control-label mb-1">Is Trending</label>
                                    <select name="is_trending" id="is_trending" class="form-control" required>
                                        @if($is_trending=='0')
                                        <option value="0" selected>No</option>
                                        <option value="1">Yes</option>
                                        @elseif($is_trending=='1')
                                        <option value="0">No</option>
                                        <option value="1" selected>Yes</option>
                                        @else
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <input type="hidden" name="id" value={{$id}} />
                </div>
            </div>

            <h2>Product Images</h2>
            <div>
                
                
                <div class="card" >
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row" id="product_images_box">
                                @php
                                    $loop_count_num=1;
                                    
                                @endphp
                                @foreach($productImagesArr as $key=>$val)
                                @php
                                    $loop_count_prev=$loop_count_num;
                                    $pIArr=(array)$val;
                                @endphp
                                <input id="piid" name="piid[]" type="hidden" value="{{$pIArr['id']}}" >
                                <div class="col-md-4 product_images_{{$loop_count_num++}}" >
                                    <label for="images" class="control-label mb-1">Product Image</label>
                                    <input id="images" name="images[]" type="file" class="form-control" aria-required="true" aria-invalid="false" required>

                                    @if($pIArr['images']!='')
                                        <a href="{{asset('storage/media/'.$pIArr['images'])}}" target=_blank>
                                            <img width="100px" src="{{asset('storage/media/'.$pIArr['images'])}}" alt="">
                                        </a>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    <label for="btn" class="control-label mb-1">&nbsp;&nbsp;&nbsp;</label><br/>
                                    @if($loop_count_num==2)
                                        <button type="button" class="btn btn-success" onclick="add_image_more()">
                                        <i class="fa fa-plus"></i>&nbsp; Add</button>
                                    @else
                                        <a href="{{url('admin/product/product_images_delete/')}}/{{$pIArr['id']}}/{{$id}}"><button type="button" class="btn btn-danger">
                                        <i class="fa fa-minus"></i>&nbsp; Remove</button></a>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h2>Product Attributes</h2>
            <div id="product_attr_box">
                
                @php
                    $loop_count_num=1;
                    
                @endphp
                @foreach($productAttrArr as $key=>$val)
                @php
                    $loop_count_prev=$loop_count_num;
                    $pAArr=(array)$val;
                @endphp
                <input id="paid" name="paid[]" type="hidden" value="{{$pAArr['id']}}" >
                <div class="card" id="product_attr_{{$loop_count_num++}}">
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="sku" class="control-label mb-1">SKU</label>
                                    <input id="sku" name="sku[]" type="text" class="form-control" aria-required="true" aria-invalid="false" value="{{$pAArr['sku']}}" required>
                                </div>
                                <div class="col-md-3">
                                    <label for="mrp" class="control-label mb-1">MRP</label>
                                    <input id="mrp" name="mrp[]" type="text" class="form-control" aria-required="true" aria-invalid="false" value="{{$pAArr['mrp']}}" required>
                                </div>
                                <div class="col-md-3">
                                    <label for="price" class="control-label mb-1">Price</label>
                                    <input id="price" name="price[]" type="text" class="form-control" aria-required="true" aria-invalid="false" value="{{$pAArr['price']}}" required>
                                </div>
                                <div class="col-md-3">
                                    <label for="size_id" class="control-label mb-1">Size</label>
                                    <select name="size_id[]" id="size_id" class="form-control">
                                        <option value="">Select Size</option>
                                        @foreach($sizes as $list)
                                            @if($pAArr['size_id']==$list->id)
                                                <option selected value="{{$list->id}}" selected>{{$list->size}}</option>
                                            @else
                                                <option selected value="{{$list->id}}">{{$list->size}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="color_id" class="control-label mb-1">Color</label>
                                    <select name="color_id[]" id="color_id" class="form-control">
                                        <option value="">Select Color</option>
                                        @foreach($colors as $list)
                                            @if($pAArr['size_id']==$list->id)
                                                <option selected value="{{$list->id}}" selected>{{$list->color}}</option>
                                            @else
                                                <option selected value="{{$list->id}}">{{$list->color}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="qty" class="control-label mb-1">Quantity</label>
                                    <input id="qty" name="qty[]" type="text" class="form-control" aria-required="true" aria-invalid="false" value="{{$pAArr['qty']}}" required>
                                </div>
                                <div class="col-md-4">
                                    <label for="attr_image" class="control-label mb-1">Product Image</label>
                                    <input id="attr_image" name="attr_image[]" type="file" class="form-control" aria-required="true" aria-invalid="false" required>

                                    @if($pAArr['attr_image']!='')
                                        <img width="100px" src="{{asset('storage/media/'.$pAArr['attr_image'])}}" alt="">
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    <label for="btn" class="control-label mb-1">&nbsp;&nbsp;&nbsp;</label><br/>
                                    @if($loop_count_num==2)
                                        <button type="button" class="btn btn-success" onclick="add_more()">
                                        <i class="fa fa-plus"></i>&nbsp; Add</button>
                                    @else
                                        <a href="{{url('admin/product/product_attr_delete/')}}/{{$pAArr['id']}}/{{$id}}"><button type="button" class="btn btn-danger">
                                        <i class="fa fa-minus"></i>&nbsp; Remove</button></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            
            
            <div>
                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                    <i class="fa fa-list-alt"></i>&nbsp;
                    <span id="payment-button-amount">Add Product</span>
                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>
            </div>
        </form>
    </div>
</div>
<script>
var loop_count=1;
function add_more(){
    loop_count++;
    var html='<input id="paid" name="paid[]" type="hidden" ><div class="card" id="product_attr_'+loop_count+'"><div class="card-body"><div class="form-group"><div class="row">';

    html+='<div class="col-md-3"><label for="sku" class="control-label mb-1">SKU</label><input id="sku" name="sku[]" type="text" class="form-control" aria-required="true" aria-invalid="false" required></div>';

    html+='<div class="col-md-3"><label for="mrp" class="control-label mb-1">MRP</label><input id="mrp" name="mrp[]" type="text" class="form-control" aria-required="true" aria-invalid="false" required></div>';

    html+='<div class="col-md-3"><label for="price" class="control-label mb-1">Price</label><input id="price" name="price[]" type="text" class="form-control" aria-required="true" aria-invalid="false" required></div>';

    var size_id_html=jQuery('#size_id').html();
    size_id_html= size_id_html.replace("selected","");
    html+='<div class="col-md-3"><label for="size_id" class="control-label mb-1">Size</label><select name="size_id[]" id="size_id" class="form-control">'+size_id_html+'</select></div>';

    var color_id_html=jQuery('#color_id').html();
    color_id_html= color_id_html.replace("selected","");
    html+='<div class="col-md-3"><label for="color_id" class="control-label mb-1">Color</label><select name="color_id[]" id="color_id" class="form-control">'+color_id_html+'</select></div>';

    html+='<div class="col-md-3"><label for="qty" class="control-label mb-1">Quantity</label><input id="qty" name="qty[]" type="text" class="form-control" aria-required="true" aria-invalid="false" required></div>';

    html+='<div class="col-md-4"><label for="attr_image" class="control-label mb-1">Product Image</label><input id="attr_image" name="attr_image[]" type="file" class="form-control" aria-required="true" aria-invalid="false" required></div>';

    html+='<div class="col-md-2"><label for="btn" class="control-label mb-1">&nbsp;&nbsp;&nbsp;</label><br/><button type="button" class="btn btn-danger" onclick=remove_more("'+loop_count+'")><i class="fa fa-minus"></i>&nbsp; Remove</button></div>';

    html+='</div></div></div></div>';
    jQuery('#product_attr_box').append(html)
}
function remove_more(loop_count){
jQuery('#product_attr_'+loop_count).remove();
}
var loop_image_count=1;
function add_image_more(){
    loop_image_count++;
    var html='<input id="piid" name="piid[]" type="hidden" value="" ><div class="col-md-4 product_images_'+loop_image_count+'"><label for="images" class="control-label mb-1">Product Image</label><input id="images" name="images[]" type="file" class="form-control" aria-required="true" aria-invalid="false" required></div>';

    html+='<div class="col-md-2 product_images_'+loop_image_count+'"><label for="btn" class="control-label mb-1">&nbsp;&nbsp;&nbsp;</label><br/><button type="button" class="btn btn-danger" onclick=remove_image_more("'+loop_image_count+'")><i class="fa fa-minus"></i>&nbsp; Remove</button></div>';

    jQuery('#product_images_box').append(html)
}

function remove_image_more(loop_image_count){
jQuery('.product_images_'+loop_image_count).remove();
}
CKEDITOR.replace('short_desc');
CKEDITOR.replace('desc');
CKEDITOR.replace('technical_specification');
</script>
@endsection