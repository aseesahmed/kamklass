@extends('admin/layout')
@section('page_title', 'Manage Tax')
@section('tax_select', 'active')
@section('container')

<div class="col-lg-12">
    <h1>Manage Tax</h1>
    <br/>
    <a href="{{url('admin/tax')}}">
        <button type="button" class="btn btn-success">Back</button>
    </a>
    <br/>
    <br/>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <form action="{{route('tax.manage_tax_process')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="tax_desc" class="control-label mb-1">Tax Description</label>
                        <input id="tax_desc" value="{{$tax_desc}}" name="tax_desc" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                        @error('tax_desc')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="tax_value" class="control-label mb-1">Tax Description</label>
                        <input id="tax_value" value="{{$tax_value}}" name="tax_value" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                        @error('tax_value')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                    </div>

                    <div>
                        <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                            <i class="fa fa-list-alt"></i>&nbsp;
                            <span id="payment-button-amount">Add Tax</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                    </div>
                    <input type="hidden" name="id" value={{$id}} />
                </form>
            </div>
        </div>
    </div>
</div>
@endsection