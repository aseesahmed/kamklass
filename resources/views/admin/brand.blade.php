@extends('admin/layout')
@section('page_title', 'Brand')
@section('brand_select', 'active')
@section('container')

<div class="col-lg-12">
    @if(session()->has('message'))
        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
            <span class="badge badge-pill badge-success">Success</span>
            {{session('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif
    <h1>Brand</h1>
    <br/>
    <a href="{{url('admin/brand/manage_brand')}}">
        <button type="button" class="btn btn-success">Add Brand</button>
    </a>
    <br/>
    <br/>
    
    <div class="table-responsive table--no-card m-b-30">
        <table class="table table-borderless table-striped table-earning">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Brand</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $list)
                <tr>
                    <td>{{$list->id}}</td>
                    <td>{{$list->name}}</td>
                    <td>
                        @if($list->image != '')
                            <img width="100px" src="{{asset('storage/media/brand/'.$list->image)}}" alt="">
                        @endif
                    </td>
                    <td>
                        <a href="{{url('admin/brand/manage_brand/')}}/{{$list->id}}"><button type="button" class="btn btn-warning">Edit</button></a>
                        @if($list->status==1)
                            <a href="{{url('admin/brand/status/0')}}/{{$list->id}}"><button type="button" class="btn btn-info">Active</button></a>
                            @elseif($list->status==0)
                                <a href="{{url('admin/brand/status/1')}}/{{$list->id}}"><button type="button" class="btn btn-info">Deactive</button></a>
                        @endif
                        <a href="{{url('admin/brand/delete/')}}/{{$list->id}}"><button type="button" class="btn btn-danger">Delete</button></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection