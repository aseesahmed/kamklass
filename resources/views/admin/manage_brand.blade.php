@extends('admin/layout')
@section('page_title', 'Manage Brand')
@section('brand_select', 'active')
@section('container')


@if($id>0)
    {{$image_required=""}}
    @else
    {{$image_required="required"}}
@endif


<div class="col-lg-12">
    @error('image')
    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
        {{$message}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    @enderror
    <h1>Manage Brand</h1>
    <br/>
    <a href="{{url('admin/brand')}}">
        <button type="button" class="btn btn-success">Back</button>
    </a>
    <br/>
    <br/>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <form action="{{route('brand.manage_brand_process')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="brand" class="control-label mb-1">Brand</label>
                        <input id="name" value="{{$name}}" name="name" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                        @error('name')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="image" class="control-label mb-1">Brand Image</label>
                        <input id="image" name="image" type="file" class="form-control" aria-required="true" aria-invalid="false" {{$image_required}}>
                        @error('image')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                        @if($image != '')
                            <img width="100px" src="{{asset('storage/media/brand/'.$image)}}" alt="">
                        @endif
                    </div>

                    <div>
                        <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                            <i class="fa fa-list-alt"></i>&nbsp;
                            <span id="payment-button-amount">Add Brand</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                    </div>
                    <input type="hidden" name="id" value={{$id}} />
                </form>
            </div>
        </div>
    </div>
</div>
@endsection